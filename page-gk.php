<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Grace Kids Page
 */
?>

<!doctype html>

<html>



<head>

  <meta charset="utf-8">

  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Grace Kids</title>

  <meta name="description" content="">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="apple-touch-icon" href="icon.png">

  <!-- Place favicon.ico in the root directory -->

  <style>html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}main{display:block}h1{font-size:2em;margin:0.67em 0}hr{box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace, monospace;font-size:1em}a{background-color:transparent}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace, monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-0.25em}sup{top:-0.5em}img{border-style:none}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type="button"],[type="reset"],[type="submit"],button{-webkit-appearance:button}[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type="button"]:-moz-focusring,[type="reset"]:-moz-focusring,[type="submit"]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:0.35em 0.75em 0.625em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{vertical-align:baseline}textarea{overflow:auto}[type="checkbox"],[type="radio"]{box-sizing:border-box;padding:0}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{-webkit-appearance:textfield;outline-offset:-2px}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details{display:block}summary{display:list-item}template{display:none}[hidden]{display:none}</style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">

  <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Dancing+Script|Open+Sans" rel="stylesheet">  

    <style>
:root {
    --accent-color: #b710a1;
    --accent-dark: #3d0435;
    --transition-a: transform 0.5s cubic-bezier(0.39,0.575,0.565,1);
}

* {box-sizing: border-box;}
img {max-width: 100%;}

body {
    margin: 0;
    font-family: "Open Sans",-apple-system,
        "BlinkMacSystemFont","Segoe UI","Roboto",
        "Helvetica","Arial",sans-serif,
        "Apple Color Emoji","Segoe UI Emoji",
        "Segoe UI Symbol";
    font-size: 18px;
}

h1 {font-size: 7.594rem;}
h2 {font-size: 5.063rem;}
h3 {font-size: 3.375rem;}
h4 {font-size: 2.25rem;}
h4 {font-size: 1.5rem;}

h1,h2,h3,h4,h5,.heading {
    font-weight: 700;
    font-family: "Bree Serif",-apple-system,
    "BlinkMacSystemFont","Segoe UI","Roboto",
    "Helvetica","Arial",sans-serif,
    "Apple Color Emoji","Segoe UI Emoji",
    "Segoe UI Symbol";
}

.container {
    padding: 20px;
    max-width: 1100px;
    margin: 0 auto;
}

.site-header .inner {
    height: 300px; 
}

.site-header .text-content {
    align-self: center;
    justify-self: center;
    color: var(--accent-color);
}

.site-header .logo {
    color: var(--accent-color);
    font-size: 1.5rem;
    text-transform: uppercase;
    letter-spacing: 2px;
}

.break {
    min-height: 400px;
    background-image: url(../images/header.jpg);
    background-size: cover;
    background-position: 0% 75%;
    display: flex;
    justify-content: center;
    align-items: center;
}
.break .container {
    max-width: 600px;
}
.break .inner {
    padding: 40px 20px;
}
.break .overlay {
    height: 100%;
    background-image: linear-gradient(to bottom, rgba(255,255,255,0.2), rgba(255,255,255,0.4));
    width: 100%;
}
.break .inner p {
    color: #fff;
    font-family: 'Dancing Script', sans-serif;
}

.intro-section {
    padding: 40px 0;
}

.intro-section .text-content {
    padding: 40px;
    height: 100%;
    width: 100%;
    background-color: rgba(183,16,161,0.6);
    opacity: 0;
    transition: all 300ms ease-out;
    min-height: 400px;
}

.intro-section .cover {
    overflow: hidden;
    position: relative;
}
.intro-section .container {max-width: 1400px;}
.intro-section .cover {
    display: flex;
    justify-content: center;
    align-items: center;
    background-size: cover;
    background-position: 50% 50%;
    transition: transform 1s cubic-bezier(0.2,1,0.8,1);
    margin: 10px;
    text-align: center;
}
.intro-section .inner--item {
    position: relative;
    top: -2rem;
}
.intro-section .inner--item:nth-child(2) {
    top: 0;
}
.intro-section .inner--item:hover .text-content {
    opacity: 1;
}
.intro-section .inner--item:hover {
    cursor: pointer;
}
.intro-section .inner--item .text-content p {
    transition: var(--transition-a);
    transform: translateY(40px);
    color: #fff;
    line-height: 1.6;
}
.intro-section .inner--item:hover .text-content p {
    transform: translateY(0);
}
.intro-section .inner--item .caption h3 {
    transition: all 600ms ease-out;
    margin-top: 10px;
}
.intro-section .inner--item:hover .caption h3 {
    color: var(--accent-color);
    transform: scale3d(0.9,0.9,1) translateY(-1rem);
}
.intro-section .inner--item:hover .cover {
    transform: scale(0.95);
    transition: transform 600ms cubic-bezier(0.2,1,0.8,1);
}
.intro-section .our-boys .cover {
    background-image: url(../images/our-boys.jpg);
}
.intro-section .our-girls .cover {
    background-image: url(../images/our-girls.jpg);
}
.intro-section p {
    line-height: 1.4;
}

.meet-our-kids {
    padding: 40px 0;
    background-color: var(--accent-dark);
    color: #fff;
}
.meet-our-kids p {
    line-height: 1.4;
}

.our-ministry .cover {
    overflow: hidden;
    height: 300px;
    width: calc(100vw / 3);
    position: relative;
    flex: 1;
}
.our-ministry .cover > .image {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-size: cover;
    display: block;
    background-position: 50% 50%;
}
.our-ministry .inner:nth-child(1) .cover:nth-child(1) > .image { background-image: url(../images/a.jpg); }
.our-ministry .inner:nth-child(1) .cover:nth-child(2) > .image { background-image: url(../images/b.jpg); }
.our-ministry .inner:nth-child(1) .cover:nth-child(3) > .image { background-image: url(../images/c.jpg); }
.our-ministry .inner:nth-child(2) .cover:nth-child(1) > .image { background-image: url(../images/d.jpg); }
.our-ministry .inner:nth-child(2) .cover:nth-child(2) > .image { background-image: url(../images/e.jpg); }
.our-ministry .inner:nth-child(2) .cover:nth-child(3) > .image { background-image: url(../images/f.jpg); }

.make-difference {
    padding: 40px 0;
}

.make-difference .kids a {
    text-decoration: none;
    color: inherit;
}

.make-difference a:hover img {
    opacity: 0.9;
}

.make-difference .and-many {
    background-color: #eee;
    margin: 10px 0;
    padding: 1rem;
}
.make-difference .and-many .table {
    font-size: 0.9rem;
    color: #333;
    column-count: 3;
    list-style: none;
    line-height: 1.3;
}

.contact-us {
    background-image: linear-gradient(to bottom, #eee, #dedede);
    padding: 40px 0;
}
.contact-us .text-content {
    flex: 3;
}
.contact-us #address {
    flex: 2;
}
#address {
    background-color: #fff;
    border-radius: 0.222rem;
    padding: 30px 20px;
    box-shadow: 0 2px 6px rgba(0,0,0,0.2);
}
#address address {font-style: normal; line-height: 1.4;}
ul {
    padding-left: 0;
}
.medium {
    font-weight: 400;
}

.white {
    color: #fff;
}

.italic {
    font-family: 'Dancing Script', sans-serif;
}

.text--center {
    text-align: center;
}

.micro-margin {
    margin-bottom: 10px;
}
.mini-margin {
    margin-bottom: 20px;
}
.big-margin {
    margin-bottom: 40px;
}
.flush {
    margin: 0;
}
.flex {display: flex;}
.flex-column {flex-direction: column;}
.flex-xcenter {justify-content: center;}
.flex-ycenter {align-items: center;}
.flex-between {justify-content: space-between;}
.flex-around {justify-content: space-around;}
.flex-xreverse {flex-direction: row-reverse;}
.flex-yreverse {flex-direction: column-reverse;}
.flex-wrap {flex-wrap: wrap;}

@media all and (max-width: 600px) {
    .palm-flex-column {
        flex-direction: column;
    }
    .palm-flex-xreverse {
        flex-direction: row-reverse;
    }
    .palm-flex-yreverse {
        flex-direction: column-reverse;
    }
    .our-ministry .cover {
        width: 100vw;
    }
    .make-difference .and-many .table {
        column-count: 1;
    }
}

@media all and (max-width: 800px) {
    .portable-flex-column {
        flex-direction: column;
    }
    .make-difference .and-many .table {
        column-count: 2;
    }
}

@media all and (min-width: 600px) {
    .break .inner p {
        font-size: 2.25rem;
        line-height: 1.4;
    }
    #address {
        margin-left: 20px;
    }
}

.cta {
    font-size: 1.5rem;
    color: var(--accent-color);
    text-decoration: none;
    cursor: text;
}

.owl-nav {
    font-size: 6rem;
}
.owl-prev {
    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
    height: 100%;
    padding: 0 1rem;
    background: rgba(0,0,0,0) !important;
}
.owl-prev:hover {
    background: rgba(0,0,0,0.05) !important;
}
.owl-prev span {
    transition: all 160ms ease-in-out;
    color: rgba(0,0,0,0.05);
    padding: 1rem;
}
.owl-prev:hover span {
    color: rgba(0,0,0,0.1);
}
.owl-next {
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    height: 100%;
    padding: 0 1rem;
    background: rgba(0,0,0,0) !important;
}
.owl-next:hover {
    background: rgba(0,0,0,0.05) !important;
}
.owl-next span {
    transition: all 160ms ease-in-out;
    color: rgba(0,0,0,0.05);
    padding: 1rem;
}
.owl-next:hover span {
    color: rgba(0,0,0,0.1);
}
    </style>
</head>



<body>


    <header class="site-header">            
      <div class="logo text--center"><h2>Grace Kids</h2></div>
      <div class="container">
          <div class="inner flex flex-column flex-ycenter">
            <div class="text-content text--center">
              <h2 class="italic medium mini-margin">Change A Child's Life</h2>
              <p class="cta">Become a sponsor today</p> 
            </div>
          </div>
      </div>
    </header>

    <main>
      <div class="break">
        <div class="overlay">
            <div class="container">
                <div class="inner text--center">
                    <p>Being loved, wanted and cared for - that is what your sponsorship 
                      provides every child that we are blessed to help. Your sponsorship 
                      covers food, uniforms, school supplies as well as access to medical 
                      care and education costs. Every child deserves to feel someone cares.</p>
                  </div>
            </div>
        </div>
      </div>
      <section class="intro-section">
        <div class="container">
          <div class="inner flex portable-flex-column space-between">
            <div class="our-girls inner--item">
              <div class="cover">
                <div class="image">
                    <div class="text-content">
                        <p>We have 8 girls, ranging from the age of 13-18. They have grown up 
                          with us for several years. We are thankful to have witnessed them 
                          grow and mature into strong young girls who aspire to pursue diverse 
                          careers. As they soon graduate high school and move into college we 
                          seek your help in allowing them to pursue their dreams and aspirations 
                          and be a light in the community.</p>
                      </div>
                </div>
              </div>
              <div class="caption text--center">
                  <h3 class="mini-margin">Our Girls</h3>
                </div>
            </div>
            <div class="our-boys inner--item">
              <div class="cover">
                <div class="image">
                    <div class="text-content">
                        <p>We have 5 boys, ranging from the age of 13-18. They have grown up with 
                          us for several years. We are thankful to have witnessed them grow and 
                          mature into strong young girls who aspire to pursue diverse careers. As 
                          they graduate high school and move into college we seek your help in 
                          allowing them to pursue their dreams and aspirations and be a light in 
                          the community.</p>
                      </div>
                </div>
              </div>
              <div class="caption text--center">
                  <h3 class="mini-margin">Our Boys</h3>
                </div>
            </div>
          </div>
        </div>
      </section>
      <section class="meet-our-kids">
        <div class="container">
          <div class="text--center">
            <h3 class="big-margin">Meet Our Kids</h3>
          </div>
          <div class="inner owl-carousel">
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/simran.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Simran</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Simran comes from a broken family. Her mother struggled to provide for Simran and her brother. Her grandmother learned of Grace and brought her grandchildren to us. Simran and her brother came to us in the year 2007. Her brother finished college and found a job for himself, Simran no the other hand has completed high school and is now in her first year of Bible College. She is faithful, dependable and a hard worker. She often helps in our church office whenever she can. 
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/Ankita01.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Ankita</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Ankita also comes from a single parent home, her mother struggled to provide for her and her siblings. As a result she also had to drop out of school until. One of our pastors ministering in the area informed her mother about Grace. And thus in 2010 Ankita joined us. Today she has graduated high school, and came first in her graduating class. She is now a freshman in college, majoring in liberal arts. 
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/upender.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Upender</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Upender came from a home inflicted with illness. His father being very ill couldn’t provide for the family, while his mother struggled to provide for her children. One of her colleagues suggested Grace to her. Upender has been with us since 2004. He recently graduated high school and is now a freshman in college majoring in Tourism Management. He is passionate about technology, and often teaches himself designing tools. 
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/Stuthi01.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Stuthi</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Stuthi is the youngest of six siblings. When their father abandoned his family, his mother worked hard to provide for her children. In the midst of her struggle, she accepted Jesus as her Savior. One of the pastors had suggested Grace to her. And thus, Stuti came to us in the year 2011. She is now a Junior in high School. She is flourishing in her classes. She is active and has an amazing sense of humor.  
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/sagar.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Sagar</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Sagar’s parents are from Nepal, his mother ran away to escape her abusive husband. When his father re-married, his step mother didn’t want much to do with him. Sagar came to Grace in 2008, he is now a sophomore in high school. He is a very creative and highly determined individual. He is in charge of lighting of our church sanctuary. 
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/rashid.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Rashid</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Rashid along with his older brother came to Grace as orphans. Since his father was very sick and couldn’t feed his family, Rashid along with his brother would steal and collect various materials to be sold for money. One of our teachers from our slum projects brought them to Grace back in 2006. He has grown much since those days, while his older brother who also grew up at Grace completed High School and went on to graduate Bible College as well.  Today, Rashid is a Junior in high school. He is an honest and hard working individual.  
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/Minnie01.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Minnie</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Minnie’s mother passed away due to an illness at a young age. Her grandmother didn’t know how to fend for her granddaughter. By God’s grace one of our pastor’s who was ministering in the area had mentioned Grace. Her grandmother immediately brought Minnie to us. And thus Minnie came to Grace in 2009. She is now almost finished with middle school, and will be entering her first year into high school.  
          </p>
                </div>
              </div>
              <div class="story">
                <div class="cover">
                  <img width="150" src="http://www.graceaogc.com/wp-content/uploads/2018/09/Priyanka01.jpg">
                </div>
                <div class="desc">
                  <h4 class="micro-margin">Priyanka</h4>
                  <div class="seperator"></div>
                  <p class="text-center">Priyanka and her sister (Priya) are both with us at Grace. Their folks passed away due an illness. Her aunt who took them in found it hard to take care of them, since she had her own children to care for. She learned of Grace and brought them to us in 2008. Today she is a junior in high school. She is a smart, a quick learner and extremely focused. 
          </p>
                </div>
              </div>
            </div>
        </div>
      </section>
      <section class="our-ministry">
        <div class="container text--center">
          <h3 class="mini-margin">OUR MINISTRY</h3>
        </div>
          <div class="inner-wrapper">
              <div class="inner flex flex-wrap">
                  <div class="cover">
                    <div class="image"></div>
                  </div>
                  <div class="cover">
                    <div class="image"></div>
                  </div>
                  <div class="cover">
                    <div class="image"></div>
                  </div>
              </div>
              <div class="inner flex flex-wrap">
                  <div class="cover">
                    <div class="image"></div>
                  </div>
                  <div class="cover">
                    <div class="image"></div>
                  </div>
                  <div class="cover">
                    <div class="image"></div>
                  </div>
              </div>
          </div>
      </section>
      <section class="make-difference">
        <div class="container">
          <div class="inner text--center">
            <h3 class="mini-margin">You can make a difference</h3>
            <p>Sponsor a child, mission or come join us on a trip of a lifetime.</p>
          </div>
          <div class="kids owl-carousel">
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/AnuragMoriya.JPG" alt="">
                <figcaption>GK01 - Anurag Moriya</figcaption>
              </figure></a>
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/GangaSagar.JPG" alt="">
                <figcaption>GK02 - Ganag Sagar</figcaption>
              </figure></a>
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/Gourav.JPG" alt="">
                <figcaption>GK03 - Gaurav</figcaption>
              </figure></a>
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/Hitesh.JPG" alt="">
                <figcaption>GK04 - Hitesh</figcaption>
              </figure></a>
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/Sonika.JPG" alt="">
                <figcaption>GK05 - Sonika</figcaption>
              </figure></a>
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/KhushiJha.JPG" alt="">
                <figcaption>GK06 - Khushi Jha</figcaption>
              </figure></a>
              <a href="https://genesispeople.com"><figure>
                <img width="300px" src="images/Rishi.JPG" alt="">
                <figcaption>GK07 - Rishi</figcaption>
              </figure></a>
          </div>
          <div class="and-many">
            <h4 class="mini-margin text--center">More kids</h4>
            <div class="table text--center">
              <li>GK01 - Anurag, 11yrs</li>
              <li>GK02 - Ganag Sagar, 11yrs</li>
              <li>GK03 - Garauv, 10yrs</li>
              <li>GK04 - Hitesh, 11yrs</li>
              <li>GK05 - Sonika, 13yrs</li>
              <li>GK06 - Khushi Jha, 12yrs</li>
              <li>GK07 - Rishi, 7yrs</li>
              <li>GK08 - Monu, 12yrs</li>
              <li>GK09 - Nandu, 10yrs</li>
              <li>GK10 - Priya, 12yrs</li>
              <li>GK11 - Jeet Bhadur, 15yrs</li>
              <li>GK12 - Priya, 10yrs</li>
              <li>GK13 - Salesh, 7yrs</li>
              <li>GK14 - Shalini, 9yrs</li>
              <li>GK15 - Sameer, 12yrs</li>
              <li>GK16 - Shashi, 11yrs</li>
              <li>GK17 - Shivam, 11yrs</li>
              <li>GK18 - Shivam, 13yrs</li>
              <li>GK19 - Sohil, 13yrs</li>
              <li>GK20 - Sumit, 12yrs</li>
              <li>and many more...</li>
            </div>
          </div>
        </div>
      </section>
      <section class="contact-us">
        <div class="container">
          <div class="flex palm-flex-column">
            <div class="text-content">
              <h3 class="mini-margin">How you can help?</h3>
              <ul>
                <li>Take some time to learn more about our kids and their stories. 
                  Plan a trip to come and visit them.</li>
                <li>By choosing to support one of our kids, you become a part of 
                  their lives. Stay connected with them and receive monthly updates 
                  from your sponsored child. Follow their journey through life.</li>
              </ul>
              <p>To learn more and sponsor a child, contact your local church.</p>
            </div>
            <div id="address">
              <h4>Contact Us:</h4>
              <address>
                <strong>5780 VIRGINIA PARKWAY,</strong> <br>
                <strong>MCKINNEY, TX 75071</strong> <br>
                Located between Ridge Rd. & Lake Forest Dr. <br>
                <a href="https://www.google.com/maps/dir//5780+Virginia+Pkwy,+McKinney,+TX+75071/@33.1995485,-96.6964948,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x864c1445a0990097:0x5e13ec3e4fda4cd7!2m2!1d-96.6943061!2d33.1995485">Get directions</a>
              </address>
            </div>
          </div>
        </div>
      </section>
    </main>

  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

  <script>
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:20,
        nav:true,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,
    });
  </script>
</body>



</html>