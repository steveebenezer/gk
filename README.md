# Grace Kids Page - Genesis Church

To build page run `jekyll build` and files will be built insite `/_site` directory ready for deployment

To host a development server `jekyll serve`

To edit your church address, replace content inside the <address> HTML tag and build page

- No Dependencies/Requirements
- Update CNAME to edit domain address
- Deploy to either [Github Pages](http://pages.github.com) / [Surge](http://surge.sh) / any host of your choice